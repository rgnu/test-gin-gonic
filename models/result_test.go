// models.article_test.go
package models

import (
	"fmt"
	"testing"
)

// Test the function that fetches all articles
func TestOk(t *testing.T) {
	obj := Ok(10)

	if !obj.IsOk() {
		t.Errorf("obj.IsOk() should be true")
	}
}

func TestErr(t *testing.T) {
	obj := Err(fmt.Errorf("Error message"))

	if !obj.IsErr() {
		t.Errorf("obj.IsErr() should be true")
	}
}

func TestOr(t *testing.T) {
	ok := Ok(10)
	err := Err(fmt.Errorf("Error message"))

	if ok.Or(err).IsErr() {
		t.Errorf("ok.Or(err) should be ok %v", ok)
	}

	if err.Or(ok).IsErr() {
		t.Errorf("err.Or(ok) should be ok %v", ok)
	}

	if err.Or(ok).Unwrap().(int) != 10 {
		t.Errorf("err.Or(ok).Unwrap() should be 10 %v", ok)
	}
}

func TestAnd(t *testing.T) {
	ok := Ok(10)
	ok2 := Ok(100)
	err := Err(fmt.Errorf("Error message"))

	if ok.And(err) != err {
		t.Errorf("ok.And(err) should be err %v", ok)
	}

	if err.And(ok) != err {
		t.Errorf("err.And(ok) should be ok %v", ok)
	}

	if ok.And(ok2).Unwrap().(int) != 100 {
		t.Errorf("ok.And(ok2).Unwrap() should be 100 %v", ok)
	}
}

func TestExpect(t *testing.T) {
	err := Err(fmt.Errorf("Error message"))

	defer func() {
		if r := recover(); r != nil {
			t.Logf("Recovered %v", r)
		}
	}()

	err.Expect(fmt.Errorf("Error from Expect"))

	t.Errorf("err.Expect() should throw a panic")
}

func TestUnwrapError(t *testing.T) {
	err := Err(fmt.Errorf("Error message"))

	defer func() {
		if r := recover(); r != nil {
			t.Logf("Recovered %v", r)
		}
	}()

	err.Unwrap()

	t.Errorf("err.Unwrap() should throw a panic")
}
