package models

import (
	"encoding/json"
	"fmt"

	"gitlab.com/rgnu/test-gin-gonic/types"
)

type Optionals []Optional

// optional model
type Optional struct {
	value types.Any
}

func None() *Optional {
	o := new(Optional)
	return o
}

func (o *Optional) Init(value types.Any) {
	o.value = value
}

func (o *Optional) IsPresent() bool {
	return o.value != nil
}

func (o *Optional) Get() types.Any {
	if !o.IsPresent() {
		return None()
	}
	return o.value
}

func (o *Optional) UnmarshalJSON(data []byte) error {
	var s interface{}
	if err := json.Unmarshal(data, &s); err != nil {
		return err
	}

	o.Init(s)

	return nil
}

func (o *Optional) MarshalJSON() ([]byte, error) {
	return json.Marshal(o.value)
}

func (o *Optional) String() string {
	return fmt.Sprint(o.value)
}

func String(value string) *Optional {
	o := new(Optional)
	o.value = &value
	return o
}
