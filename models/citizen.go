package models

import (
	"fmt"

	"gitlab.com/rgnu/test-gin-gonic/apis"
	"gitlab.com/rgnu/test-gin-gonic/types"
)

type citizen struct {
	apis.Person
	country string
}

func Citizen(name string, country string, address apis.Address) apis.Citizen {
	self := new(citizen)

	self.Person = Person(name, address)
	self.SetCountry(country)

	return self
}

func (self *citizen) GetCountry() string {
	return self.country
}

func (self *citizen) SetCountry(country string) {
	self.country = country
}

func (self *citizen) Nationality() types.String {
	return fmt.Sprintf("%s is a citizen of %s", self.GetName(), self.GetCountry())
}

// IPerson Delegation
// func (self *citizen) GetName() string {
// 	return self.person.GetName()
// }

// func (self *citizen) GetAddress() IAddress {
// 	return self.person.GetAddress()
// }

// func (self *citizen) Location() {
// 	self.person.Location()
// }

func (self *citizen) Talk() types.String {
	return fmt.Sprintf("Hello, my name is %s and I'm from %s", self.GetName(), self.GetCountry())
}
