package models

type Result struct {
	ok  interface{}
	err error
}

func Ok(ok interface{}) *Result {
	o := new(Result)
	o.ok = ok
	return o
}

func Err(e error) *Result {
	o := new(Result)
	o.err = e

	return o
}

func (o *Result) IsOk() bool {
	return o.err == nil
}

func (o *Result) IsErr() bool {
	return o.err != nil
}

func (o *Result) Unwrap() interface{} {
	if o.IsErr() {
		panic(o.err)
	}

	return o.ok
}

func (o *Result) Or(res *Result) *Result {
	if o.IsOk() {
		return o
	}

	return res
}

func (o *Result) And(res *Result) *Result {
	if o.IsOk() {
		return res
	}

	return o
}

func (o *Result) Expect(err error) interface{} {
	if o.IsErr() {
		panic(err)
	}

	return o.Unwrap()
}
