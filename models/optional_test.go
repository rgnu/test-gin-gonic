// models.optional_test.go
package models

import (
	"encoding/json"
	"testing"
)

// Test the function that fetches all articles
func TestOptionalNone(t *testing.T) {
	obj := None()

	if obj.IsPresent() {
		t.Errorf("IsPresent in None should allways return false")
	}
}

func TestOptionalStringNoEmpty(t *testing.T) {
	obj := String("test")

	if !obj.IsPresent() {
		t.Errorf("IsPresent in String should allways return true")
	}
}

func TestOptionalUnmarshalJSON(t *testing.T) {
	var obj Optionals
	data := `["Federico", null]`

	if err := json.Unmarshal([]byte(data), &obj); err != nil {
		t.Fatal(err)
	}

	if !obj[0].IsPresent() {
		t.Errorf("obj[0].IsPresent() should be true %v", obj)
	}

	if obj[1].IsPresent() {
		t.Errorf("obj[1].IsPresent() should be false %v", obj)
	}
}
