package models

import "gitlab.com/rgnu/test-gin-gonic/apis"

type address struct {
	number string
	street string
	city   string
	state  string
	zip    string
}

func Address(number, street, city, state, zip string) apis.Address {
	self := new(address)

	self.number = number
	self.street = street
	self.city = city
	self.state = state
	self.zip = zip

	return self
}

func (self *address) GetNumber() string {
	return self.number
}

func (self *address) GetStreet() string {
	return self.street
}

func (self *address) GetCity() string {
	return self.city
}

func (self *address) GetState() string {
	return self.state
}

func (self *address) GetZip() string {
	return self.zip
}
