// models.citizen_test.go
package models

import (
	"testing"
)

func TestCitizen(t *testing.T) {
	name := "Ricardo Alfonsin"
	number := "1118"
	street := "Foo Street"
	city := "Barcelona"
	state := "Mendoza"
	cp := "1234"
	country := "Argentina"
	address := Address(number, street, city, state, cp)

	obj := Citizen(name, country, address)

	if obj.GetName() != name {
		t.Fatalf("Name() should return %s", name)
	}

	if obj.GetAddress() != address {
		t.Fatalf("Address() should return %v", address)
	}

	if result := obj.Nationality(); result != "Ricardo Alfonsin is a citizen of Argentina" {
		t.Fatalf("Nationality() should return '%s' not '%s", "Ricardo Alfonsinis a citizen ofArgentina", result)
	}
	if result := obj.Talk(); result != "Hello, my name is Ricardo Alfonsin and I'm from Argentina" {
		t.Fatalf("Talk() should return '%s' not '%s'", "Hello, my name is Ricardo Alfonsin and I'm from Argentina", result)
	}

	if result := obj.Location(); result != "I’m at 1118 Foo Street Barcelona Mendoza 1234" {
		t.Fatalf("Talk() should return '%s' not '%s'", "I’m at 1118 Foo Street Barcelona Mendoza 1234", result)
	}
}
