package models

import (
	"fmt"

	"gitlab.com/rgnu/test-gin-gonic/apis"
)

type salary struct {
	basic     int
	insurance int
	allowance int
}

// Salary model
type Salary = *salary

// NewSalary Salary constructor
func NewSalary(basic, insurance, allowance int) apis.Salary {
	return &salary{
		basic:     basic,
		insurance: insurance,
		allowance: allowance,
	}
}

// GetBasic salary
func (o Salary) GetBasic() int {
	return o.basic
}

// GetInsurance salary
func (o Salary) GetInsurance() int {
	return o.insurance
}

// GetAllowance salary
func (o Salary) GetAllowance() int {
	return o.allowance
}

// String representation of Salary model
func (o Salary) String() string {
	return fmt.Sprintf(
		"Salary{basic=%d, insurance=%d, allowance=%d}",
		o.GetBasic(), o.GetInsurance(), o.GetAllowance(),
	)
}
