package models

import (
	"fmt"

	"gitlab.com/rgnu/test-gin-gonic/apis"
)

type employee struct {
	firstName, lastName string
	salary              apis.Salary
}

// Employee model
type Employee = *employee

// NewEmployee Employee contructor
func NewEmployee(firstName, lastName string, salary apis.Salary) apis.Employee {
	return &employee{
		firstName: "Ross",
		lastName:  "Geller",
		salary:    salary,
	}
}

// GetFirstName return the Employee firstname
func (o Employee) GetFirstName() string {
	return o.firstName
}

// GetLastName return the Employee lastname
func (o Employee) GetLastName() string {
	return o.lastName
}

// GetFullName return the Employee fullname = firstname + lastname
func (o Employee) GetFullName() string {
	return fmt.Sprintf("%s %s", o.GetFirstName(), o.GetLastName())
}

// GetSalary return the Employee salary
func (o Employee) GetSalary() apis.Salary {
	return o.salary
}

// String representation of Employee
func (o Employee) String() string {
	return fmt.Sprintf(
		"Employee{firtsName=%s, lastName=%s, salary=%s}",
		o.GetFirstName(), o.GetLastName(), o.GetSalary(),
	)
}
