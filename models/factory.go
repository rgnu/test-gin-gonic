package models

import (
	"gitlab.com/rgnu/test-gin-gonic/apis"
)

type employeeFactory struct{}

func newEmployeeFactory() apis.EmployeeFactory {
	return &employeeFactory{}
}

func (o *employeeFactory) GetRossSalary() apis.Salary {
	return NewSalary(1100, 50, 50)
}

func (o *employeeFactory) GetRossEmployee() apis.Employee {
	return NewEmployee(
		"Ross",
		"Geller",
		o.GetRossSalary(),
	)
}

// EmployeeFactory implementation
var EmployeeFactory = newEmployeeFactory()
