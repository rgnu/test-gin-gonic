// models.person_test.go
package models

import (
	"testing"
)

func TestPerson(t *testing.T) {
	name := "Ricardo Alfonsin"
	number := "1118"
	street := "Foo Street"
	city := "Barcelona"
	state := "Mendoza"
	cp := "1234"
	address := Address(number, street, city, state, cp)

	obj := Person(name, address)

	if obj.GetName() != name {
		t.Fatalf("Name() should return %s", name)
	}

	if obj.GetAddress() != address {
		t.Fatalf("Address() should return %v", address)
	}

	if result := obj.Talk(); result != "Hi, my name is Ricardo Alfonsin" {
		t.Fatalf("Talk() should return '%s' not '%s'", "Hi, my name is Ricardo Alfonsin", result)
	}

	if result := obj.Location(); result != "I’m at 1118 Foo Street Barcelona Mendoza 1234" {
		t.Fatalf("Talk() should return '%s' not '%s'", "I’m at 1118 Foo Street Barcelona Mendoza 1234", result)
	}
}
