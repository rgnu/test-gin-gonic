// models.address_test.go
package models

import (
	"testing"
)

func TestAddress(t *testing.T) {
	number := "1118"
	street := "Foo Street"
	city := "Barcelona"
	state := "Mendoza"
	cp := "1234"
	obj := Address(number, street, city, state, cp)

	if obj.GetCity() != city {
		t.Fatalf("City should be %s", city)
	}

	if obj.GetNumber() != number {
		t.Fatalf("Number should be %s", number)
	}

	if obj.GetState() != state {
		t.Fatalf("State should be %s", state)
	}

	if obj.GetStreet() != street {
		t.Fatalf("Street should be %s", street)
	}
}
