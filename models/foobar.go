package models

import "fmt"

type Celsius = int

type Mutable = bool

type IFoobar interface {
	GetTemperature() Celsius
	SetTemperature(value Celsius)
	GetMutable() Mutable
	SetMutable(value Mutable)
	String() string
}

type TFoobar struct {
	mutable     Mutable
	temperature Celsius
}

func Foobar(options ...func(IFoobar) error) (IFoobar, error) {
	self := new(TFoobar)

	// Default values...
	_ = Default(self)

	// Option paremeters values:
	for _, op := range options {
		if err := op(self); err != nil {
			return nil, err
		}
	}

	err := Validate(self)

	return self, err
}

func Default(self IFoobar) error {
	self.SetMutable(true)
	self.SetTemperature(37)
	return nil
}

func Validate(self IFoobar) error {
	if self.GetTemperature() > 100 {
		return fmt.Errorf("temperature should be lower than 100 celsius")
	}

	return nil
}

func ReadonlyFlag(self IFoobar) error {
	self.SetMutable(false)
	return nil
}

func Temperature(t Celsius) func(IFoobar) error {
	return func(self IFoobar) error {
		if t > 100 {
			return fmt.Errorf("temperature should be lower than 100 celsius")
		}

		self.SetTemperature(t)
		return nil
	}
}

// Setters and Getters
func (self *TFoobar) GetTemperature() Celsius {
	return self.temperature
}

func (self *TFoobar) SetTemperature(value Celsius) {
	self.temperature = value
}

func (self *TFoobar) GetMutable() Mutable {
	return self.mutable
}

func (self *TFoobar) SetMutable(value Mutable) {
	self.mutable = value
}

func (self *TFoobar) String() string {
	return fmt.Sprintf("Foobar{mutable=%t,temperature=%d}", self.GetMutable(), self.GetTemperature())
}
