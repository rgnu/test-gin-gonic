package value

import (
	"encoding/json"
	"testing"
)

func TestValueSet(t *testing.T) {
	obj := NewValue("test")

	if obj.GetAsString() != "test" {
		t.Errorf("GetAsString should allways return test")
	}

	obj.Set("test-v2")
	if obj.GetAsString() != "test-v2" {
		t.Errorf("GetAsString should allways return test-v2")
	}
}

func TestValueNil(t *testing.T) {
	obj := Nil()

	if obj.Get() != nil {
		t.Errorf("Nil() should allways return nil")
	}

	if obj.IsPresent() {
		t.Errorf("Nil().IsPresent() should allways return false")
	}
}

func TestValueString(t *testing.T) {
	obj := String("test")

	if obj.GetAsString() != "test" {
		t.Errorf("String().GetAsString should allways return true")
	}
}

func TestValueInteger(t *testing.T) {
	obj := Integer(10)

	if obj.GetAsInteger() != 10 {
		t.Errorf("Integer().GetAsInteger() should return 10")
	}
}

func TestValueFloat(t *testing.T) {
	obj := Float(1.1)

	if obj.GetAsFloat() != 1.1 {
		t.Errorf("Integer().GetAsFloat() should return 1.1")
	}
}

func TestValueBoolean(t *testing.T) {
	obj := Boolean(true)

	if obj.GetAsBoolean() != true {
		t.Errorf("Integer().GetAsBoolean() should return true")
	}
}

func TestValueUnmarshalJSON(t *testing.T) {
	var obj Values
	data := `["Federico", null]`

	if err := json.Unmarshal([]byte(data), &obj); err != nil {
		t.Fatal(err)
	}

	if !obj[0].IsPresent() || obj[0].GetAsString() != "Federico" {
		t.Errorf("obj[0].GetAsString() should return Federico %v", obj[0])
	}

	if obj[1] != nil {
		t.Errorf("obj[1] should be <nil> %v", obj[1])
	}
}
