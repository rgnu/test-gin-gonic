package value

import (
	"encoding/json"
	"fmt"

	"gitlab.com/rgnu/test-gin-gonic/types"
)

type Values []Value

// Value model
type Value = *value

type value struct {
	value types.Any
}

/**
 * Nil ...
 */
func Nil() Value {
	return NewValue(nil)
}

/**
 * String ...
 */
func String(value types.String) Value {
	return NewValue(value)
}

/**
 * Integer ...
 */
func Integer(value types.Integer) Value {
	return NewValue(value)
}

/**
 * Float ...
 */
func Float(value types.Float) Value {
	return NewValue(value)
}

/**
 * Boolean ...
 */
func Boolean(value types.Boolean) Value {
	return NewValue(value)
}

func NewValue(v types.Any) Value {
	self := new(value)
	self.value = v
	return self
}

func (self Value) IsPresent() types.Boolean {
	return self.value != nil
}

func (self Value) Set(v types.Any) Value {
	self.value = v
	return self
}

func (self Value) Get() types.Any {
	return self.value
}

func (self Value) GetAsString() types.String {
	result := self.Get().(types.String)
	return result
}

func (self Value) GetAsInteger() types.Integer {
	result := self.Get().(types.Integer)
	return result
}

func (self Value) GetAsFloat() types.Float {
	result := self.Get().(types.Float)
	return result
}

func (self Value) GetAsBoolean() types.Boolean {
	result := self.Get().(types.Boolean)
	return result
}

func (self Value) UnmarshalJSON(data []byte) error {
	var s types.Any
	if err := json.Unmarshal(data, &s); err != nil {
		return err
	}

	self.Set(s)

	return nil
}

func (self Value) MarshalJSON() ([]byte, error) {
	return json.Marshal(self.value)
}

func (self Value) String() string {
	return fmt.Sprint(self.value)
}
