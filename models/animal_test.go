// models.animals_test.go
package models

import (
	"encoding/json"
	"testing"
)

func TestAnimal(t *testing.T) {
	blob := `["gopher","armadillo","zebra","unknown","gopher","bee","gopher","zebra"]`
	var zoo []Animal

	if err := json.Unmarshal([]byte(blob), &zoo); err != nil {
		t.Fatal(err)
	}

	census := make(map[Animal]int)
	for _, animal := range zoo {
		census[animal]++
	}

	if census[Gopher] != 3 {
		t.Fatalf("Gopher animals should be equals to 3, counted %d", census[Gopher])
	}

	if census[Unknown] != 3 {
		t.Fatalf("Unknown animals should be equals to 3, counted %d", census[Unknown])
	}

	result, err := json.Marshal(zoo)
	if err != nil {
		t.Fatal(err)
	}

	expected := `["gopher","unknown","zebra","unknown","gopher","unknown","gopher","zebra"]`
	if string(result) != expected {
		t.Fatalf("%s should be equals to %s", result, expected)
	}
}
