package models

import (
	"fmt"

	"gitlab.com/rgnu/test-gin-gonic/apis"
	"gitlab.com/rgnu/test-gin-gonic/types"
)

type person struct {
	name    string
	address apis.Address
}

func Person(name string, address apis.Address) apis.Person {
	self := new(person)
	self.Init(name, address)
	return self
}

func (self *person) Init(name string, address apis.Address) {
	self.name = name
	self.address = address
}

func (self *person) GetName() string {
	return self.name
}

func (self *person) GetAddress() apis.Address {
	return self.address
}

func (self *person) Talk() types.String {
	return fmt.Sprintf("Hi, my name is %s", self.GetName())
}

func (self *person) Location() types.String {
	address := self.GetAddress()

	return fmt.Sprintf(
		"I’m at %s %s %s %s %s",
		address.GetNumber(),
		address.GetStreet(),
		address.GetCity(),
		address.GetState(),
		address.GetZip(),
	)
}
