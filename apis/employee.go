package apis

// Employee interface
type Employee interface {
	GetFirstName() string
	GetLastName() string
	GetFullName() string
	GetSalary() Salary
}
