package apis

import "gitlab.com/rgnu/test-gin-gonic/types"

// Human interface
type Human interface {
	Talk() types.String
}
