package apis

// Salary interface
type Salary interface {
	GetBasic() int
	GetInsurance() int
	GetAllowance() int
}
