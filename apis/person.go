package apis

import "gitlab.com/rgnu/test-gin-gonic/types"

// Person interface
type Person interface {
	Human
	GetName() types.String
	GetAddress() Address
	Location() types.String
}
