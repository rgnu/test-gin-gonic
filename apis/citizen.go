package apis

// Citizen interface
type Citizen interface {
	Person
	GetCountry() string
	Nationality() string
}
