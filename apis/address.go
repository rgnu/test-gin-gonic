package apis

// Address interface
type Address interface {
	GetNumber() string
	GetStreet() string
	GetCity() string
	GetState() string
	GetZip() string
}
