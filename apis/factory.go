package apis

// EmployeeFactory interface
type EmployeeFactory interface {
	GetRossEmployee() Employee
	GetRossSalary() Salary
}
