package types

// Any type
type Any = interface{}

type AnyArray = []Any

type StringAnyMap = map[String]Any

type String = string

type Boolean = bool

type Integer = int64

type Float = float64

type Error = error
