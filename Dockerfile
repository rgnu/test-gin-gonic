FROM golang:1.11 as build-env

WORKDIR /go/src/gitlab.com/rgnu/test-gin-gonic

COPY . .

RUN make all

# final stage
FROM alpine:3.7

COPY --from=build-env /go/src/gitlab.com/rgnu/test-gin-gonic/templates /app/templates
COPY --from=build-env /go/src/gitlab.com/rgnu/test-gin-gonic/*.exe /app/

WORKDIR /app

ENV PORT=8081

EXPOSE $PORT

CMD ["/app/server.exe"]