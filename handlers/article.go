// Package handlers :
package handlers

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/rgnu/test-gin-gonic/models"
)

type showIndexPageResponse struct {
	Title   string           `json:"title"`
	Payload []models.Article `json:"payload"`
}

type getArticleResponse struct {
	Title   string          `json:"title"`
	Payload *models.Article `json:"payload"`
}

// ShowIndexPage :
func ShowIndexPage(c *gin.Context) {
	articles := models.GetAllArticles()

	// Call the HTML method of the Context to render a template
	render(c, showIndexPageResponse{
		Title:   "Home Page",
		Payload: articles,
	}, "index.html")

}

// GetArticle :
func GetArticle(c *gin.Context) {
	// Check if the article ID is valid
	if articleID, err := strconv.Atoi(c.Param("id")); err == nil {
		// Check if the article exists
		if article, err := models.GetArticleByID(articleID); err == nil {
			// Call the HTML method of the Context to render a template
			c.HTML(
				// Set the HTTP status to 200 (OK)
				http.StatusOK,
				// Use the index.html template
				"article.html",
				// Pass the data that the page uses
				getArticleResponse{
					Title:   article.Title,
					Payload: article,
				},
			)
		} else {
			// If the article is not found, abort with an error
			c.AbortWithError(http.StatusNotFound, err)
		}

	} else {
		// If an invalid article ID is specified in the URL, abort with an error
		c.AbortWithStatus(http.StatusNotFound)
	}
}
