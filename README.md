# Test Gin Gonic

## Build Docker image

```sh
$ docker build -t test-gin-gonic .
```

### Run Docker image
```sh
$ docker run --rm -ti -p 8081:8081 test-gin-gonic
```

### Get Articles List
```sh
$ curl localhost:8081/articles
```

### Get Article By Id
```sh
$ curl localhost:8081/articles/1
```
