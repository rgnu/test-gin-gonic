GO?=go
MKDIR:=mkdir -p
RMRF:=rm -rf
BINDIR:=bin
CMD := $(addsuffix .exe, $(notdir $(wildcard $(CURDIR)/cmd/*)))

all: $(CMD)

clean:
	$(RMRF) $(CMD)

vendor: go.mod go.sum
	GO111MODULE=on $(GO) mod vendor && touch $@

test-unit: vendor
	$(GO) test -v ./...

%.exe: ./cmd/% vendor
	GO111MODULE=on CGO_ENABLED=0 $(GO) build -a -ldflags '-extldflags "-static"' -o $@ ./$<
