package main

import (
	"fmt"
	"reflect"

	"gitlab.com/rgnu/test-gin-gonic/types"
)

type ID = types.String

type Namespace = types.String

type ServiceID = types.String

type PlanID = types.String

type PlatformURI = types.String

type Replicas = types.Integer

type Tag = types.String

type Tags = []Tag

// ServiceInstance interfacee
type ServiceInstance interface {
	GetID() ID
	GetServiceID() ServiceID
	GetPlanID() PlanID
	GetNamespace() Namespace
	GetParams() types.StringAnyMap
	GetParam(k string) types.Any
}

// AdapterInstance interface
type AdapterInstance interface {
	ServiceInstance
	GetPlatformURI() PlatformURI
	GetReplicas() Replicas
	GetTags() Tags
}

// ServiceInstanceImpl model
type ServiceInstanceImpl struct {
	id        ID
	serviceID ServiceID
	planID    PlanID
	namespace Namespace
	params    types.StringAnyMap
}

// NewServiceInstanceImpl ServiceInstance contructor
func NewServiceInstanceImpl(id ID, serviceID ServiceID, planID PlanID, namespace Namespace, params types.StringAnyMap) ServiceInstance {
	o := new(ServiceInstanceImpl)

	o.id = id
	o.serviceID = serviceID
	o.planID = planID
	o.namespace = namespace
	o.params = params

	return o
}

// GetID method
func (o *ServiceInstanceImpl) GetID() ID {
	return o.id
}

// GetServiceID method
func (o *ServiceInstanceImpl) GetServiceID() ServiceID {
	return o.serviceID
}

// GetPlanID method
func (o *ServiceInstanceImpl) GetPlanID() PlanID {
	return o.planID
}

// GetNamespace method
func (o *ServiceInstanceImpl) GetNamespace() Namespace {
	return o.namespace
}

// GetParams method
func (o *ServiceInstanceImpl) GetParams() types.StringAnyMap {
	return o.params
}

// GetParam method
func (o *ServiceInstanceImpl) GetParam(k string) types.Any {
	if result, ok := o.params[k]; ok {
		return result
	}
	return nil
}

// AdapterInstanceImpl model
type AdapterInstanceImpl struct {
	ServiceInstance
}

// NewAdapterInstanceImpl contructor
func NewAdapterInstanceImpl(s ServiceInstance) AdapterInstance {
	o := new(AdapterInstanceImpl)
	o.ServiceInstance = s

	return o
}

// GetPlatformURI method
func (o *AdapterInstanceImpl) GetPlatformURI() PlatformURI {
	return o.GetParam("platformUri").(PlatformURI)
}

// GetReplicas method
func (o *AdapterInstanceImpl) GetReplicas() Replicas {
	return o.GetParam("replicas").(Replicas)
}

// GetTags method
func (o *AdapterInstanceImpl) GetTags() Tags {
	var tags Tags

	items := reflect.ValueOf(o.GetParam("tags"))

	for i := 0; i < items.Len(); i++ {
		tags = append(tags, items.Index(i).Interface().(string))
	}

	return tags
}

// AdapterLogic interface
type AdapterLogic interface {
	Provision(a AdapterInstance) error
}

// AdapterLogicImpl model
type AdapterLogicImpl struct{}

// NewAdapterLogicImpl constructor
func NewAdapterLogicImpl() AdapterLogic {
	o := new(AdapterLogicImpl)
	return o
}

// Provision method
func (o *AdapterLogicImpl) Provision(a AdapterInstance) error {
	fmt.Printf(
		"Provision Adapter [Id:%v] [Namespace:%v] [PlatformURI:%v] [Replicas: %v] [Tags: %v]\n",
		a.GetID(), a.GetNamespace(), a.GetPlatformURI(), a.GetReplicas(), a.GetTags(),
	)
	return nil
}
