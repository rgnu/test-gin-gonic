// https://spf13.com/post/is-go-object-oriented/
// https://www.sohamkamani.com/blog/golang/2018-06-20-golang-factory-patterns/
// https://medium.com/@_jesus_rafael/composing-interfaces-in-go-58980969e897
package main

import (
	"encoding/json"
	"fmt"

	"gitlab.com/rgnu/test-gin-gonic/apis"
	"gitlab.com/rgnu/test-gin-gonic/models"
	"gitlab.com/rgnu/test-gin-gonic/types"
)

func SpeakTo(h apis.Person) {
	h.Talk()
}

type AddressRecord struct {
	Number string `json:"number"`
	Street string `json:"street"`
	City   string `json:"city"`
	State  string `json:"state"`
	Zip    string `json:"zip"`
}

type PersonRecord struct {
	Name    string        `json:"name"`
	Address AddressRecord `json:"address"`
}

func AddressFromRecord(data AddressRecord) apis.Address {
	return models.Address(data.Number, data.Street, data.City, data.State, data.Zip)
}

func PersonFromRecord(data PersonRecord) apis.Person {
	return models.Person(data.Name, AddressFromRecord(data.Address))
}

func PersonFromJSON(data []byte) apis.Person {
	obj := PersonRecord{}
	if err := json.Unmarshal(data, &obj); err != nil {
		panic(err)
	}

	return PersonFromRecord(obj)
}

func main() {
	astr := `{"name": "Federico", "address": {"number": "293"}}`

	c := models.Citizen("Steve", "America", models.Address("13", "Main", "Gothan", "NY", "01313"))
	c.Talk()
	c.Nationality()
	c.Location()

	p := models.Person("Rocky", models.Address("20", "Rivadavia", "CABA", "BA", "3000"))
	p.Talk()
	p.Location()

	a := PersonFromJSON([]byte(astr))
	a.Location()

	fmt.Println(">> SpeakTo ------------------")
	SpeakTo(c)
	SpeakTo(p)
	SpeakTo(a)

	fmt.Println(">> Test Adapter ------------")

	service := NewServiceInstanceImpl(
		"1", "2", "3", "default",
		types.StringAnyMap{"platformUri": "http://test", "replicas": 2, "tags": types.AnyArray{"foo", "bar"}},
	)
	adapter := NewAdapterInstanceImpl(service)
	logic := NewAdapterLogicImpl()

	_ = logic.Provision(adapter)
}
