package main

import (
	"fmt"

	"gitlab.com/rgnu/test-gin-gonic/models"
)

// main
func main() {
	factory := models.EmployeeFactory
	ross := factory.GetRossEmployee()

	fmt.Println(ross)
}
