package main

import (
	"fmt"

	m "gitlab.com/rgnu/test-gin-gonic/models"
)

func main() {
	f1, err := m.Foobar()
	fmt.Println(f1, err)

	f2, err := m.Foobar(m.Temperature(25))
	fmt.Println(f2, err)

	f3, err := m.Foobar(m.Temperature(10), m.ReadonlyFlag)
	fmt.Println(f3, err)

	f4, err := m.Foobar(m.ReadonlyFlag, m.Temperature(10))
	fmt.Println(f4, err)

	f5, err := m.Foobar(m.Temperature(1000))
	fmt.Println(f5, err)
}
