package main

import (
	"gitlab.com/rgnu/test-gin-gonic/handlers"
)

func initializeRoutes() {

	// Handle the index route
	router.GET("/articles", handlers.ShowIndexPage)
	router.GET("/articles/:id", handlers.GetArticle)
}
