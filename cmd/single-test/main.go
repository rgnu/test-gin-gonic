package main

import (
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/rgnu/test-gin-gonic/handlers"
)

// DB : user database
var DB = make(map[string]string)

func setupRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	r := gin.Default()

	r.GET("/hello", handlers.HelloWorld)

	// Ping test
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, NewPongResponse())
	})

	// Get user value
	r.GET("/user/:name", func(c *gin.Context) {
		user := c.Params.ByName("name")

		if value, ok := DB[user]; ok {
			c.JSON(200, NewUserResponse(user, value))
		} else {
			c.JSON(404, NewNotFoundResponse())
		}
	})

	// Authorized group (uses gin.BasicAuth() middleware)
	// Same than:
	// authorized := r.Group("/")
	// authorized.Use(gin.BasicAuth(gin.Credentials{
	//	  "foo":  "bar",
	//	  "manu": "123",
	//}))
	authorized := r.Group("/", gin.BasicAuth(gin.Accounts{
		"foo":  "bar", // user:foo password:bar
		"manu": "123", // user:manu password:123
	}))

	authorized.POST("admin", func(c *gin.Context) {
		user := c.MustGet(gin.AuthUserKey).(string)
		// Parse JSON
		json := NewUserRequest()

		if c.Bind(json) == nil {
			println(json.Value)
			DB[user] = json.Value
			c.JSON(200, NewOkResponse())
		}
	})

	return r
}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = ":8081"
	}

	DB["foo"] = "value"
	r := setupRouter()
	// Listen and Server in 0.0.0.0:8081
	r.Run(port)
}
