package main

type statusResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

type StatusResponse = *statusResponse

func NewStatusResponse(status string, message string) StatusResponse {
	return &statusResponse{Status: status, Message: message}
}

func NewOkResponse() StatusResponse {
	return NewStatusResponse("ok", "")
}

// NewNotFoundResponse create a new Not Found Response
func NewNotFoundResponse() StatusResponse {
	return NewStatusResponse("error", "Not Found")
}

type pongResponse struct{}

type PongResponse = *pongResponse

func NewPongResponse() PongResponse {
	return &pongResponse{}
}

type userResponse struct {
	User  string `json:"user"`
	Value string `json:"value"`
}

type UserResponse = *userResponse

func NewUserResponse(user string, value string) UserResponse {
	return &userResponse{User: user, Value: value}
}

type userRequest struct {
	Value string `json:"value" binding:"required"`
}

type UserRequest = *userRequest

func NewUserRequest() UserRequest {
	return &userRequest{}
}
